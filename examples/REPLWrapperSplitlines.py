#! /usr/bin/python3

import pexpect
import pexpect.replwrap
import sys
print(sys.argv[1:])
for line in sys.argv[3].splitlines( ):
    print(line)
c = pexpect.replwrap.REPLWrapper(
        pexpect.spawnu(
            sys.argv[1],
            echo=False,
            logfile=sys.stdout
        ),
        sys.argv[2],
        None
    )
for line in sys.argv[3].splitlines():
    c.run_command(line)
# .splitlines() is useless because pexpect is doing it already!
# https://pexpect.readthedocs.io/en/stable/_modules/pexpect/replwrap.html
