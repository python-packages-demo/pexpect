# pexpect

[**pexpect**](https://pypi.org/project/pexpect)
![PyPI - Downloads](https://img.shields.io/pypi/dm/pexpect)
Spawning child applications

![](https://qa.debian.org/cgi-bin/popcon-png?packages=python-pexpect%20python3-pexpect%20expect%20libtest-expect-perl&show_installed=on&want_legend=on&want_ticks=on&date_fmt=%25Y-%25m&beenhere=1)

[![Packaging status](https://repology.org/badge/vertical-allrepos/pexpect.svg)](https://repology.org/project/pexpect/versions)
[![Packaging status](https://repology.org/badge/vertical-allrepos/python:pexpect.svg)](https://repology.org/project/python:pexpect/versions)
